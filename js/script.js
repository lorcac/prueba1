$(function () {


    $(".gallery img").on("click", function (e) {

        var item = e.target;
        var info = $(item).data("info");
        var src = $(item).attr("src");
        var alt = $(item).attr("alt");
        var image = $(".modal-image").find("img");
        var description = $(".modal-image").find("p");

        $(image).attr("src", src);
        $(image).attr("alt", alt);
        $(description).html(info);
    });

});