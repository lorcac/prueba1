
$.validator.addMethod("frun", function (value, element) {
    return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value);
}, "Debe ingresar un formato de run valido");

$.validator.addMethod("justtext", function (value, element) {
    return this.optional(element) || /^[a-zA-Z ]/.test(value);
}, "Debe ingresar solo texto");

$("#formulariol").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        run: {
            required: true,
            frun: true,
            max: 12
        },
        nombre: {
            required: true,
            justtext: true
        },
        fecha_nacimiento: {
            required: true,
            maxage: 2002
        },
        telefono: {
            number: true
        }
    },
    messages: {
        email: {
            required: "El correo es requerido",
            email: "Debe ingresar un correo electrónico con el formato correcto"
        },
        run: {
            required: "El RUN es requerido",
            frun: "Debe ingresar un RUN válido",
            max: ""
        },
        nombre: {
            required: "El nombre es requerido",
            justtext: "Debe ingresar solo texto"
        },
        fecha_nacimiento: {
            maxage:  "Año debe ser menor a " + 2002,
            required: "La fecha de nacimiento es requerida",
        },
        telefono: {
            number: "Debe ingresar solo números"
        }
    }
});